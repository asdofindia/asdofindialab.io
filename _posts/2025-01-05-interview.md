---
layout: post
title: Interview
tags:
- social
---

##### What is an interview? What to do in one? #####

I have been part of several interviews — more often as *interviewer* than as *interviewee*. Here I will list down my philosophy in and around interviews.

### Power

There are several reasons for an interview to happen. It could be:
* A is offering a job, B is looking for a job.
* A is looking to buy something, B is selling something.
* A wants to know something, B has some answers.
* A and B are strangers who are getting to know each other.

It is sometimes not easy to spot, but there's a power dynamics that plays out in any interview, even in the last scenario above.

And one might think the power is always higher for those who are asking the questions. But it need not always be. Power has some funny properties:
* Power does not come from vacuum, it exists in continuum with the society we live in
* Power exists regardless of whether it is used
* Nevertheless, power that is used is what often counts
* Power is a social construct, it exists only between individuals, not within individuals

So when A is offering a job and B is looking for a job, the power between A and B depends a lot on who A and B are, but also a lot on the society around A & B. That is to say, although it might look like a job interview is purely about the vacancy in A's company and B's skills, there is much more to it. If there are too many job seekers in the "market", A gets a higher hand. Vice versa, if B is insanely skilled, B has higher bargaining capacity. If the culture of the sector is where employees are treated badly, it automatically becomes weaker for B to standup. Whereas if the culture is a bit more egalitarian, then A & B will be forced to talk on equal terms.

Typical human beings are aware of these power differences subconsciously and/or consciously. And therefore, it doesn't require that power be actually used for power to play its effect.

A good demonstration of power dynamics is what happened to Indian media post-2014. When UPA was in power (pun intended), they yielded a lot of power to citizens and democratic processes. Hence, media could ask very strong questions and be critical of the government. But when NDA came to power, the power was taken back and consolidated. Consequently, press freedom went down and the media could not ask critical questions in any interviews.

This is the power game. Power can be yielded or wielded and how you use it determines your success in the game.[^wield]

[^wield]: It might appear like using your power is always to one's advantage and yielding it is pointless. Not really. Power is fragile. If you use too much of it, you can break relationships and lose all your power. If you need a book recommendation, I suggest *Turn The Ship Around!: A True Story of Turning Followers into Leaders* by L. David Marquet which deal with the whole question of power in leadership.

### Information asymmetry

If power hierarchy is so straightforward, what is even the point of an interview, one might wonder. This is where the whole "revealing your cards" come in.

Though in general terms we can talk about power as if it is an objective thing, the exact calculations of how much power an individual holds is not a value we can look-up in a key-value store. Most interviews therefore are like interviews between strangers. Each side is trying to find information about the other side to make assessments (including how much power they have). An interview is about decreasing unknowns.

By revealing information about one's strengths, one gets assessed as stronger by the other party, and vice versa. This is why a "portfolio" becomes a critical part of interviews. By showcasing your strengths in a portfolio, you are making it easier for the other party to have trust in your skills.

When A knows everything about B, and B knows not much about A (or vice versa), there is information asymmetry. When there is information asymmetry, the decisions that are made can be sub-optimal. If either party is unethical they can use information asymmetry to make bad decisions to their advantage.

For example, in a job interview if a candidate knows nothing about a topic, but somehow manages to convince the interviewer that they are really good on the topic, a wrong hiring decision might be made.

Also, if a company is a shitty place to work at, but during the process of interview the candidate is led to believe that it is an awesome place to work at, a wrong decision to sign-up could be made.

### My perspective

I look at interviews like I look at the beginning of any other relationship in life.

* I don't like power games
* I don't like liars

Or in positive terms

* I believe in egalitarian relationships
* I believe in honesty in communication

This philosophy works only when working with other people who hold compatible philosophies. If I apply these in an environment where people play power games or lie, I suffer (as I have learnt the hard way). "Corporate" is a word used to describe such hostile environments. Perhaps it is because of the stereotype that in capitalistic enterprises everyone is trying to get ahead by all means. But this is largely a matter of human beings. Even within capitalistic companies there can be islands where good managers can create a nice work environment. And vice versa, even within small non-profits there can be toxic workspaces.

Here is how this perspective plays out in my life and interact with other philosophies of mine.

My life is an open book. My [about page](/about/) reveals too much information about my past, for example. There are perhaps some things I can't publicly write about which are not on my blog. But I have written [even about my bank balance](https://blog.learnlearn.in/money-matters/). A lot of this is consistent with how I [value and advocate](https://blog.learnlearn.in/my-obsession-with-free-knowledge/) free and open source software, open science, open educational resources, creative commons, and other aspects of the free culture.

I value social justice and equity over capitalistic notions of success. I hate hierarchies. I try to be an anti-caste feminist. I work for health equity (and health is such a broad topic you can have a healthy world only when everything else is fixed).

I realize I am already sitting over wealth that far exceeds the average in several million households in India. Fortunately, my parents are independent and self-sufficient. [Swathi and I](https://actionforequity.in/team/) chose to be childfree. Therefore I don't care too much about making money for myself.

I try to fill my life with packets of joy. I find joy in learning things and solving problems. I find joy in helping people level up. I find joy in social impact. Therefore I try to find some or the other aspect of this joy in all of my work.

So what do I do in an interview?

I judge the philosophy of the people I'm talking to and the work they're doing. Sometimes people seem to have the right philosophy, but the work they're doing may be in the wrong framework. In those situations I consider actions to be speaking louder than words - i.e., the work speaks louder than the words.

If they're from the opposite end of the spectrum, I run away. If running is not an option, I play their own game and shut them off. For example, when people come talk to me to build some random digital health app, I ask for compensation that is near my "market value". That automatically turns people away.

If they're people working for the same goals as I am, I don't think about anything else and jump to start the work. Because at that point, it becomes a joint entrepreneurship. This is how I'm with [Nivarana](https://nivarana.org), and I'm with [DemTech](https://demtech.ai). (Also, DemTech is looking for early career developers. [Contact me](/about/#contact) if you're interested.)