---
layout: post
title: "Dr C Viswanathan's Comment on 'Different Systems of Medicine' in mfc's 50 year celebration"
tags:
- rationality
---

##### Verbatim quoting the comment made by CVN #####

<iframe width="560" height="315" src="https://www.youtube.com/embed/L0_tqC5rCAw?si=M3BSs-RE3OfEQjD6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I am sure that my stand in this topic is a minority opinion. I am also certain that this opinion would be met more with ridicule and resistance than with approval in this forum.

My position is that the so called "different systems of medicine" should have no place out of "History of ideas" in a modern society. I know, many people don't like the word 'modern'. By the term "modern society", I mean a society that upholds the values of "liberty, equality, fraternity and justice". These values, which we call constitutional values, are really values of 'Enlightenment' - or 'European Enlightenment' if you insist.

Enlightenment project is a sociopolitical project that nobody likes in today's India. The right wing hates it, the left wing has only contempt for it. Of course, this was always a minority's concern in our country, historically speaking. Unlike the 'National movement', this 'movement' - if you can call it that name - came up from the 'depressed castes'. If I am to name a few 'big names' that would represent this camp, almost everyone comes from the Dalit-Bahujan background. Mahatma Phule, Babasaheb Ambedkar, Thanthai Periyar, Mahatma Ayyankali, Sahodaran Ayyappan - all from the depressed castes. This is a movement that developed as a response to the day-to-day existential insults heaped upon the depressed caste people over millenia.

For them, European Enlightenment ideas, brought to India, of course along with British colonial domination, provided hope. Take any of the names I mentioned. Mahatma Phule was directly inspired by the book 'Rights of Man' by Thomas Paine. Mahatma Ayyankali's cherished ambition was to see ten youngsters from his community to get BA degrees.

I would just state one short sentence as the basic epistemological position of what I may call the 'enlightenment camp'. It is this: "Science (broadly construed) is the only way of knowing." I know this would arouse a sense of disbelief and even revulsion in people from the Ashish Nandi-Vandana Shiva school. They would call this 'Scientism' I am almost certain. I agree. This is scientism. And, I stand by it!

Even for those not much invested in that particular school of thought, this position may appear to be somewhat strange and even wrong.

This is probably not the time or place to discuss in detail about the epistemological position called scientism. For anyone who is interested, I would suggest reading of articles on this subject by the young Belgian philosopher, Maarten Boudry. His article, "Scientism schmientism! Why there are no other ways of knowing apart from science (broadly construed)" is available online.

Thank you.
