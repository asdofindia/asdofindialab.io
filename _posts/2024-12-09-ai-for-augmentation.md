---
layout: post
title: "Use AI for Augmentation, Not for Replacement"
tags:
- ai
---

##### AI is a tool for augmentation, not replacement. But unfortunately many humans use it as a replacement, and therein lies the problem. #####

In the past many months, I have been observing the effects of AI (LLMs) in various aspects of software development. There is good news and bad news. The good news is that AI shifts the paradigm of work and raises the level of what is possible. The bad news is that to unlock this possibility, people and the society need to use AI correctly by changing their own paradigms of how to work. 

As a polyglot programmer who rarely remembers the syntax of all languages, it is incredibly useful to be able to tell LLMs to generate code in languages that I can read and confirm.

But the LLMs today are not very good at building good architecture for code. So I have to very precisely guide it on how to architect the code. It can very well implement the functions, but I have to tell it how to implement.

On another level, I do take architecture suggestions from LLMs too. But I start the conversation specifically by talking about architecture in plain English. This way the LLMs can give me the best suggestions based on the blog posts and documentations that it has read.

I have seen several engineers jump directly from an abstract concept to code through LLMs. They just give it a one-line explanation of what needs to be done, and copy paste the code it generates. This might seem to work. In fact, this is probably the same as how coding used to be before LLMs for them, copy-pasting from StackOverflow. But this doesn't work. The architecture in such code will usually be shitty. And you end up with sub-standard code overall.

The takeaway is that you cannot let AI be doing everything on its own. You've got to understand what it is doing and direct it to your liking. (The same advice held true for StackOverflow! You cannot just copy paste from StackOverflow, you've to understand what's happening.)

You have to think of AI as a collaborator who is really smart in some ways, and really stupid in some ways. You leverage the strengths of AI by asking it to do some thinking. Then, when it comes back, you do some thinking of your own and guide it in the right direction. And at some point, you decide it is enough and get the work out into concrete forms (like codebase).

If you use AI as a replacement for your thinking, you make a fool of yourself. If you use AI to augment your thinking, you become more efficient.
