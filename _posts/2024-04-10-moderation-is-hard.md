---
layout: post
title: "Moderation is Hard. Moderation is Very Important."
tags:
  - culture
---

##### Moderation, CoC, etc are very important. But they're also very hard. To be a moderator is as difficult a job as it is to be a leader #####

Let's start with three to four case scenarios.

### Indian Pirates and Right-wing "Trolls"

[Indian Pirates](https://pirates.org.in/) had an open discussion forum on matrix. Once a couple of right-wing "trolls" got onto it. I spent hours and hours debating with them about issues like erasure/denial of transgender identity. They simply wouldn't engage earnestly. I stopped participating in Indian Pirates. Later Indian Pirates decided to make the group only for members.

### FOSS United and Elon Musk

FOSS United's telegram group had a crazy few weeks a couple of months ago. There was one of the directors posting content that is in awe of Elon Musk and that style of capitalism. These predictably encouraged people to spout more EM-like non-sense.

One of those was this OSM contributor (let's call them NO) who was linking to conservative view-points from United States. I got frustrated and literally said "Thanks for sharing, Karen". I was rightly called out by another person asking "Isn't karen a slur?". I kept silent and escaped.

Later NO posted a couple of links that were directly denying that transgender identity exists. I deleted those using my admin privileges and commented the same.

A couple of women (who were even speakers at FOSS United events) left the group because of all these messages being encouraged. Afterwards one of the staff at FOSS United banned NO citing reports of [Code of Conduct violation](https://forum.fossunited.org/t/guidelines-for-discussion-on-telegram-and-forum/1373):

> You missed the first point of the guideline: "Be Nice".

I didn't really think ban was a good idea. But nevertheless, I messaged this staff and congratulated them for taking strict action.

### FSCI and People Questioning Free Software

[FSCI](https://fsci.in/)'s discussion channels are on XMPP and Matrix. They're bridged to each other and this bridge is unreliable. Considering these, most people who join these are people who already have an understanding of free software. And the general amount of diversity of thoughts and consequent heated discussions is low.

But nevertheless there are times when people who have too much experience realize that free software doesn't solve all the problems and start questioning some of the axioms.

Like a few months ago an artist who has been contributing a lot to free software (let's call them KK) was venting about how it is very difficult to use free software for their work - because certain features they need are missing, etc. And some people who didn't really understand where KK is coming from started questioning KK as to why they're complaining about software that is coming to them for "free". This made KK felt unheard and KK kept coming back with more examples of the problem.

A particular mod (let's call them AP) floated the idea of banning KK. KK left the group.

In the past couple of weeks, an apparently new member (let's call them C4) was talking about how WhatsApp ensures privacy more than unencrypted XMPP rooms and was pushing Signal's "Ecosystem is Moving" blog post. C4 had joined via Matrix. Several people wrote points against C4's points — from XMPP. I'm still in the middle of investigating which messages didn't come through the bridge, but several messages did get dropped. In the confusion that ensued, there was again the idea of ban. 

It appeared like C4 wasn't responding to counter-arguments and kept stating the same things. This was being defined as "trolling" and there was a proposal to amend Code of Conduct to include this as unacceptable behaviour. C4 did get banned. But some other mods felt the ban was unnecessary and this was reversed.

But this confusion is continuing even today with C4 writing more messages from their point of view and others writing more messages from their point of view.

### Swades and the Neutral POV

Swades is a whatsapp group of some public health people in India. There's this person (let's call them BJ) who wants to post "the opposite side" arguments for every thing. When someone talks about Arvind Kejriwal's arrest, BJ talks about how all parties are corrupt. When someone talks about hospital in Gaza being bombed, BJ brings up an Israeli press release about "Hamas hiding in hospital".

There's not been any moderator action here, but several others have commented and asked BJ hard questions to bring it to BJ's notice that what they're doing is unacceptable.

### Freedesktop and Hyprland

There's [this post](https://blog.vaxry.net/articles/2024-fdo-and-redhat) written by Hyprland dev about receiving emails from freedesktop CoC team about issues in hyprland's discord. The back and forth is all documented there. Hyprland dev (HD) thinks it is intimidation as their discord is unrelated to freedesktop. Freedesktop CoC team thinks it is well within their rights to protect freedesktop contributors everywhere.

### The hard problem of moderation

I have been a moderator in many online groups for several years. I have also been a participant in many heated discussions. I am quite political and quite unafraid to call people out. Here's what I think:

**Moderation is hard. Moderation is very important.**

Moderation is quite like leadership. There are no guidebooks, there are no ready-made solutions. Every situation is new. Every situation is different. Every problem is hard. Every day you grow older.

I would like to talk about some useful insights though.

### Moderation is important

There are several tasks moderators do. The easiest is to keep spam/vandal/bots etc out of online groups.

But the more difficult task is to "moderate" actual participants of the community/conversation into particular goals.

If goals have to be met, it is important that conversations are moderated.

When a group is very small or when a group doesn't have any particular goal, this doesn't really matter. But the communities I'm talking about are all large and have formed around particular goals.

Without moderation, conversations go round and round or drift away into related useless topics. Without moderation the politics of a community gets diluted by people who don't believe in those politics. Without moderation, all kinds of emotions remain unaddressed and people start shouting or withdrawing. 

A good moderator is constantly feeling the pulse of the community members and helping people relax into the vibe of the community.

### Moderators are humans

We have to realize that moderators are human beings who come from among all of us. They would have all human shortcomings including cognitive biases, complexes, and so on. Which means, quite often moderators make errors too.

Sometimes I see the errors that moderators make used as argument against moderation in total. That's counter-productive. We need to call out errors of mods and make them and their moderation better.

### Code of Conduct is hard too

The code of conduct serves at least two purposes. It is a political document that makes it clear how invested a particular group is in countering society's entrenched values. It is also a source of power for moderators to act. Without a code of conduct, every action would have to be debated from scratch.

But all that said, a code of conduct is only as good as the moderators. The interpretation of code of conduct has elements of subjectivity (which I'll discuss below). And the implementation of it is highly dependent on the politics of the moderators. For this blog post we can consider code of conduct and moderation to be interchangeable terms.

### Yes, it is all politics

It is difficult to go into depth in a single blog post. But eventually all of this is about politics and political worldviews. We have to develop an understanding of capitalism, socialism, conservatism, liberalism, Ambedkarism, Gandhianism, Sangh ideology, feminism, Marxism and so on to understand what's happening within our tiny microcosms.

Take free software, for example. It exists in stark contradiction with capitalism. The core spirit of free software is humanity and sharing. The core spirit of capitalism is money. The world is a mix of many worldviews. For any one particular worldview to be promoted, there has to be strategies played out. Free software communities tend to focus on the benefit of sharing software and push for more people to understand the dangers of proprietary software. This is an inherently political process. One cannot see proprietary software as dangerous unless they see the dangers of capitalism and surveillance state. Politics is the process of making people realize the narrowness of their worldview. It is about making people think from our perspectives and to negotiate what we think is needed.

If a free software community doesn't engage in this politics, it will quickly devolve into a group that has no particular ambition and be co-opted into the dominant mode of functioning of the world.

### There's an element of truth in all politics

I've [written about this previously](https://blog.learnlearn.in/2021/10/how-to-live-with-opposition.html). Human beings have good brains. They come from various life experiences. The politics they hold is essentially the result of their life experiences. It is useful to note that there's an element of truth in all worldviews and that an intersectional approach of understanding the world as a complex mess is a productive and respectful approach.

### The moderator's role is to avoid wasting everyone's time

While it is all politics and everyone needs to discuss things to develop better understanding and all of that, infinite discussions are not productive. It leads to a situation where things that need to get done aren't getting done.

The moderator's role is to intervene so that people move forward. If someone is stuck in their worldview and not willing to see a different perspective despite the best effort from others, the moderator has to recognize the futility of the situation and help everyone out.

But there are times when it is possible that someone is learning and developing their politics. The moderator has to facilitate this political growth.

This is an off-the-cuff decision. The moderator has to feel the pulse and act impulsively. And that's where often errors emerge from. A moderator can prevent a lot of errors by reading widely and being very sensitive to people's emotional states. There are also several conversation tactics they can use which helps. I'm gonna try and write about some of them at the end of this post.

### Diversity is not a waste of time

It is extremely useful to have multiple points of view. Debates might have to be stopped. But do not stop diverse viewpoints. It is those points of view which will inform your own point of view.

### Reverse -isms

I've [written about this particular issue before](../reverse-sexism/). 

Several groups (especially those with code of conduct) come from the ideology that there is an "oppressed" class and an "oppressor" class. From this emerges the idea that you cannot be sexist towards a man, you cannot be casteist towards a privileged caste person, and so on.

This causes a lot of confusion. You get [things like "men's rights activism"](../why-anti-feminists/).

Intersectionality as a framework clears this confusion, but unfortunately it is a complicated framework and requires lots of political maturity to adopt. Remember all of us are humans.

Granted that's the state of matters, it is statistically better to err on the side of the "oppressed". Especially because reverse-ism issues are usually not brought up in good faith.

Therefore, avoid explicitly validating reverse-ism claims, but do keep in mind that the claim could be true.

### The extent of CoC

Remember NO from above? They're very active in OSM groups. What do I do there? Should I push for them to get banned from OSM community too?

Similarly, in FSCI there are people with lots of bullshit written in their blog. Should I push and get them banned?

This is a very hard question.

If someone posts a technical write-up on their blog and shares that, and in the same blog there's a transphobic post, what do we do? If that person is celebrated within the group, doesn't it make it difficult for let's say a trans person in the group?

One tactic that can be used is to use private messages to extent support to the trans people in the group and say "Listen, I believe their stuff is transphobic and I do not approve of it." But this is hard because not all trans people might be openly trans.

If we start a public conversation about that topic, we might be unintentionally causing Streisand effect and cause more transphobic content to come out in the open.

We might choose to write a long email to the person who wrote the blog, but that might or might not be productive.

The ideal solution is to increase doing trans-affirmative things in the group. Yes, this one person might have something on their blog that's transphobic, but we make the whole group so obviously trans-friendly that this one person will feel the heat.

But we don't live in fantasy land. It is not as easy to convert a silent majority to visibly trans-affirmative or queer affirmative or feminist.

## Love as a framework

It is useful for moderators (and anyone) to adopt a framework of love. Use the language of love. Make it a point to talk to everyone like they're your friends.

Yes, it is difficult. (I have myself lost my cool often.) But keep practising.

And by love, I mean as bell hooks says in "all about love".

>  “The will to extend one’s self for the purpose of nurturing one’s own or another’s spiritual growth… Love is as love does. Love is an act of will-namely, both an intention and an action. Will also implies choice. We do not have to love. We choose to love.” 

### Conversation tactics that moderators should use

* Anticipate miscommunication and correct them. Ask clarifying questions. Rephrase messages. Give people more context (publicly or privately).
* [Choose positive reinforcements over punishments](https://blog.learnlearn.in/2023/09/dont-shoot-your-colleagues.html). Use words like "I love that", "I agree with ..."
* Even those who believe they are hyper-rational respond to caring words.
* Keep in touch with people who are involved. Use private messages.
* Summarize points in long debates, let people restart from a common point (or rest).
* Talk about emotions. Use words like "frustration", "disappointment", "sadness", etc. Label emotions. If you're labelling others' emotions, please be careful to not frame it negatively. For example, say "I feel frustration in your voice. Is that correct?"
* Silence.
