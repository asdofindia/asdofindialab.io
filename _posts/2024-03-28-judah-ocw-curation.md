---
layout: post
title: "Judah's Curation of OCW courses"
---

##### Judah spammed me with a list of courses from OCW (probably related to technology, yes, I asked for it). So I decided to review them. #####

### [Designing and Sustaining Technology Innovation for Global Health Practice](https://ocw.mit.edu/courses/hst-939-designing-and-sustaining-technology-innovation-for-global-health-practice-spring-2008/)

This is a course that's ideal for someone bringing the hustle culture into healthcare. It talks about the experiences of PATH (HPV Vaccine case study), microfluidics, new systems of drug delivery, alternative energy sources, medical device development, EMRs, biopharmaceuticals, vaccine development, etc. The focus seems to be on creating entrepreneurs in captialistic system, taking them through the nuances of funding, profit, etc.

The most interesting page is [the list of related resources](https://ocw.mit.edu/courses/hst-939-designing-and-sustaining-technology-innovation-for-global-health-practice-spring-2008/pages/related-resources/) which has some grants, blogs, etc listed.

This course only has student notes, and some reading links.

### [Internet Technology in Local and Global Communities](https://ocw.mit.edu/courses/ec-s01-internet-technology-in-local-and-global-communities-spring-2005-summer-2005/pages/syllabus/)

This is a 2005 "course" helping prepare people from MIT to visit Africa and teach people there programming, etc. It has a slideset on teaching tips (pretty basic), on [cross-cultural communication](https://ocw.mit.edu/courses/ec-s01-internet-technology-in-local-and-global-communities-spring-2005-summer-2005/resources/mitec_s01s05_l8_cros_cul/) (very nice!). There's also a self-reference to OCW.

The rest of the material includes a set of slides introducing Java (the programming languages, not the island). But I think there are better resources to learn Java.

It is not very clear if the people who took this course were learning Java for the first time through this course (and then teaching Africans a few months later). If so, the audacity of Americans!

### [How to Make (Almost) Anything](https://ocw.mit.edu/courses/mas-863-how-to-make-almost-anything-fall-2002/)

This is about laser cutters and PCBs and stuff like that from 2002. I definitely think it is easier to learn these in 2024.

### [Disease and Health: Culture, Society, and Ethics](https://ocw.mit.edu/courses/21a-215-disease-and-health-culture-society-and-ethics-spring-2012/)

This is a very interesting course (timeless). It is about medicine, but about the art of medicine. "For instance, about the role of ritual in healing, morality, keeping the world in balance (preventive medicine)". There is a lot of reading through medical anthropology here. Unfortunately, the reading suggestions aren't clearly documented. They are referring to the names Farmer, Luhrmann, Lock, Fadiman.

The course starts with basic terminologies like "medicalization", then goes on to look at the role of religion, symbols, culture, and so on...

I'm gonna go through the lecture notes of these once again, afterwards, slowly, because even at a quick glance it gave me thoughts like "if 'depression' isn't a diagnosis in say an urban slum culture, then what does that mean for mental health in such places?"

### [Introduction to Technology and Policy](https://ocw.mit.edu/courses/esd-10-introduction-to-technology-and-policy-fall-2006/)

This is a very interesting course which breaks down the policy process. Going through some of the material, like [policy strategy instruments](https://ocw.mit.edu/courses/esd-10-introduction-to-technology-and-policy-fall-2006/resources/lec4/) made me really think about the syllabus of this (with respect to the skill set I need to develop in policy engagement)

### [Technopolitics, Culture, Intervention](https://ocw.mit.edu/courses/4-647-technopolitics-culture-intervention-fall-2014/)

The introduction to this course itself is using too much jargon. It says it is about architecture (makes sense :D). There are too many readings listed (with more jargon), but no lecture notes. Useless.

### [Water and Sanitation Infrastructure in Developing Countries](https://ocw.mit.edu/courses/11-479j-water-and-sanitation-infrastructure-in-developing-countries-spring-2007/)

This was very interesting because SOCHARA's C-WASH program is essentially devoted to the same problem. And Bangalore's Slum Development Unit is one of the lectures! I could even see a photo from "Anandapuram" (I'm not sure if it is the same Anandapuram SOCHARA works in). This lecture also gives the complexity of the problem, including this [reading material](http://web.archive.org/web/20071023095214/http://www.wsp.org/filez/pubs/319200735514_Connectingtheslums.pdf). The rest of it goes to technical details, and this could be an excellent course for new associates in C-WASH. The last lecture is on various on-site shit handling methods, for example.

### [Introduction to Computers in Public Management II](https://ocw.mit.edu/courses/11-208-introduction-to-computers-in-public-management-ii-january-iap-2002/)

This is another dated course (workshop). But the method used maybe modernized for a present-day course in using computers to solve public health problems.

### [Logistics and Supply Chain Management](https://ocw.mit.edu/courses/esd-273j-logistics-and-supply-chain-management-fall-2009/)

This is a very technical course on how to do logistics and supply chain management (as it says in the title). Things like handling inventory, figuring out where to put warehouse, etc.

### [Medical Decision Support](https://ocw.mit.edu/courses/hst-951j-medical-decision-support-fall-2005/)

This is a full blown mathematics course for AI. I'm pretty sure there are newer courses that are easier to follow (and with a better ratio of explanation to math)

### [Housing and Human Services](https://ocw.mit.edu/courses/11-421-housing-and-human-services-spring-2005/)

This is about homelessness and how to deal with it. The lecture notes is interesting because there's only one page of it. But there are several readings.

### [Health Information Systems to Improve Quality of Care in Resource-Poor Settings](https://ocw.mit.edu/courses/hst-s14-health-information-systems-to-improve-quality-of-care-in-resource-poor-settings-spring-2012/)

The first course on the list with videos! But this course is also a bit like the first course (hustle), just that the focus is purely information systems. There's some lectures about OpenMRS and stuff like that. Some videos about quality improvement - stuff like checklists, monitoring, lean sigma, blah blah.

### [Innovation Systems for Science, Technology, Energy, Manufacturing, and Health](https://ocw.mit.edu/courses/sts-081-innovation-systems-for-science-technology-energy-manufacturing-and-health-spring-2017/)

This one also has videos. It seems very "economics". Questions like how does innvoation happen? How do things get built? I want to come back and go through this carefully.

### [Out of Context: A Course on Computer Systems That Adapt To, and Learn From, Context](https://ocw.mit.edu/courses/mas-963-out-of-context-a-course-on-computer-systems-that-adapt-to-and-learn-from-context-fall-2001/)

Something something context. The syllabus of this seems interesting (especially the part about UI design and just-in-time context). But only three slides are available. There's some reading list too.

### [Organizations as Enacted Systems: Learning, Knowing and Change](https://ocw.mit.edu/courses/15-963-organizations-as-enacted-systems-learning-knowing-and-change-fall-2002/)

The preface of this course is so very interesting "We view organizations as enacted systems, wherein humans are continually shaping the structures that influence their action in turn. In other words, we create the systems that then create us."

But there is simply no lecture notes, or lecture details in this course.

There are some readings though. Might as well read them.

### [Decision Making in Large Scale Systems](https://ocw.mit.edu/courses/2-997-decision-making-in-large-scale-systems-spring-2004/)

This seems very algorithm/ML/AI/math-y. Skipping.

### [Introductory Digital Systems Laboratory](https://ocw.mit.edu/courses/6-111-introductory-digital-systems-laboratory-fall-2002/)

Fun course on building basic digital systems. [nand2tetris](https://www.nand2tetris.org/) might be more relevant. Of course, this has some different content.

### [Planning Communication](https://ocw.mit.edu/courses/11-914-planning-communication-spring-2007/)

The most important part of this is [this lecture on effectively doing a presentation](https://ocw.mit.edu/courses/11-914-planning-communication-spring-2007/resources/xavbriefing/). The rest of the course is people practising this.

### [Cultures of Computing](https://ocw.mit.edu/courses/21a-350j-cultures-of-computing-fall-2011/)

There are no lecture notes here. If you need a huge reading list on the history/culture of computing, go [here](https://ocw.mit.edu/courses/21a-350j-cultures-of-computing-fall-2011/pages/readings/)

### [Intentional Public Disruptions: Art, Responsibility, and Pedagogy](https://ocw.mit.edu/courses/res-11-002-intentional-public-disruptions-art-responsibility-and-pedagogy-fall-2017/)

Very interesting title, huh! This course is specifically centered on the method practiced by Stephen Carpenter using "art" to disrupt people's thinking and force them to think about certain issues (like race). There are videos. Must watch (later).

### [Community-Owned Enterprise and Civic Participation](https://ocw.mit.edu/courses/11-954-community-owned-enterprise-and-civic-participation-spring-2005/)

The reading list might be useful in exploring community-owned models like co-operatives. Relevant for [prav](https://prav.app) perhaps?

### [The Anthropology of Cybercultures](https://ocw.mit.edu/courses/21a-850j-the-anthropology-of-cybercultures-spring-2009/)

Interesting title, but there's only reading list and more reading suggestions in syllabus. The problem with reading lists is that if we had so much time to read, this whole course wouldn't be necessary.

### [Development, Planning, and Implementation: The Dialectic of Theory and Practice](https://ocw.mit.edu/courses/11-s940-development-planning-and-implementation-the-dialectic-of-theory-and-practice-fall-2015/)

Again, no lecture notes. Lots of reading. The development and planning here are about urban development, urban planning, etc.

### [Research Topics in Architecture: Citizen-Centered Design of Open Governance Systems](https://ocw.mit.edu/courses/4-285-research-topics-in-architecture-citizen-centered-design-of-open-governance-systems-fall-2002/)

Some kind of attempt to make people build a governance software and learn about governance, something like that. The lecture notes are very terse. Not really useful.

### [Techno-identity: Who we are and how we perceive ourselves and others](https://ocw.mit.edu/courses/mas-963-techno-identity-who-we-are-and-how-we-perceive-ourselves-and-others-spring-2002/)

Again, no lecture notes. There are lecture wise reading materials. But the topic seems very archane. What's with these people?

### [Disaster, Vulnerability and Resilience](https://ocw.mit.edu/courses/11-941-disaster-vulnerability-and-resilience-spring-2005/)

This is a really interesting course, especially considering climate disasters all around. Talks about risks, resilience, and so on. Has lecture notes and readings. Gotta come back to this later.

### [Healthy Cities: Assessing Health Impacts of Policies and Plans](https://ocw.mit.edu/courses/11-s941-healthy-cities-assessing-health-impacts-of-policies-and-plans-spring-2016/)

Erm, looks dull.

### [Architecture and Communication in Organizations](https://ocw.mit.edu/courses/15-990-architecture-and-communication-in-organizations-fall-2003/)

This is some workshop or conference or something. Anyhow, most courses on architecture seem pointless to me.

### [Advanced Urban Public Finance: Collective Action and Provisions of Local Public Goods](https://ocw.mit.edu/courses/11-902-advanced-urban-public-finance-collective-action-and-provisions-of-local-public-goods-spring-2009/)

Promising topic. Has some nice readings. Gotta come back to this (like the earlier course on community-owned enterprises)

### [New Global Agenda: Exploring 21st Century Challenges through Innovations in Information Technologies](https://ocw.mit.edu/courses/17-918-new-global-agenda-exploring-21st-century-challenges-through-innovations-in-information-technologies-january-iap-2006/)

This is from 2006, okay? It is talking about lasers, cellular phones (sic), blogsites (sic) and so on as the latest. There's discussion on governance of these. But I'm too bored already.


### Conclusion

I've been developing a theory about learning/education that makes me rethink online courses. I've mainly been ignoring them after 12th standard, with the idea that most things can be learnt by action-reflection cycles. But good courses help accelerate those learnings (without wasting years to learn hard lessons). So, I've gotta start looking at more courses and online learning experiences.






