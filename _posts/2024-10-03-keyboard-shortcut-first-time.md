---
layout: post
title: "Cancel and do it with the keyboard shortcut"
tags:
  - tips
---

##### When you discover a keyboard shortcut for what you're about to do, cancel doing that and do it with the keyboard shortcut instead. #####

Many pieces of software include a helpful keyboard shortcut next to many menu entries or in the item tooltip. For example, when you open "File" in Firefox Desktop, you see "Save Page As...     Ctrl+S".

Often you'll see these shortcuts when you have already reached the place with your mouse or navigation.

You might have an urge to just continue with what you were about to do (manually activate the option) and "keep the shortcut in mind for the next time".

I say, don't do that. Remember the shortcut. Cancel what you were doing. And use the shortcut immediately to do it instead.

That way, you'll train your muscle right there!
