---
layout: post
title: "Is Open-Source Maintainers Burnout an Example of the Tragedy of the Commons?"
tags:
  - culture
---

##### The tragedy of the commons is invoked every time something wrong happens with open-source development. Is that always correct? #####

"I'm giving up — on open source", that's the title of a [blog post written by a developer](https://nutjs.dev/blog/i-give-up) announcing why they're giving up on open-source development because the users of their project(s) demanded free labour. Best summarized in:

> And if people start insulting you for something you're doing in your free time, it's time to stop.

This is seen as an example of "the tragedy of the commons" idea by some. In this post, we will examine if that's indeed the case.

### The tragedy of the commons

The wikipedia page on [commons](https://en.wikipedia.org/wiki/Commons) and [Tragedy of the commons](https://en.wikipedia.org/wiki/Tragedy_of_the_commons) are both good reads.

Commons refers to things that belong to nobody in particular and are "common" to everyone.

Commons can occur in two ways. It can arise naturally when it is impossible to prevent others from accessing something. This is the case with air, water, planet, etc. You can't (easily) prevent someone from "using" air (by breathing). You can't keep people away from rivers or the sea.

Or it can arise when people decide to come together and share things with everyone, like when you have a park that's open to everyone, or when you build a community library.

So what is the tragedy of the commons? Let's hear from Garrett Hardin who popularized the term talking about grazing cows in commons of grassland:

> Therein is the tragedy. Each man is locked into a system that compels him to increase his herd without limit – in a world that is limited. Ruin is the destination toward which all men rush, each pursuing his own best interest in a society that believes in the freedom of the commons.

So the idea is that
1. Each person or at least some person(s) will act selfishly.
2. They will over-utilize the commons to the detriment of it.
3. The commons will be ruined.

### Criticism of the tragedy of the commons

Before going into this specifically, we can look at one particular key concept of "classical economics" that shines through the field of economics, [changing people who study it uncritically as well](https://www.npr.org/2017/02/21/516375434/does-studying-economics-make-you-selfish): self-interest. Much of economics is calculated on the idea that people act in self-interest. But this is not always the case! There are plenty of people who act altruistically. There are plenty of people who do the fair thing. It is an unfortunate side effect of capitalism and capitalists teaching people what to think that people think selfishness is what's natural or right.

That aside, there's plenty of people calling "the tragedy of the commons" idea itself bullshit (which you should have read in wikipedia by now).

The main disagreement is on point 2 above. For someone to over-utilize commons to the detriment of it, they should have unfettered access to it. In the real world, we see that this is not the case (at least in most cases). For example, Elinor Ostrom [found this in field study](https://commonslibrary.org/practising-commoning/#Key_Concepts):

> Elinor Ostrom, 1990 has documented in many places around the world how communities devise ways to govern the commons to assure its survival for their needs and future generations. A classic example of this was her field research in a Swiss village where farmers tend private plots for crops but share a communal meadow to graze their cows. While this would appear the perfect model to prove the tragedy-of-the-commons theory, Ostrom discovered that in reality there were no problems with overgrazing. That is because of a common agreement among villagers that one is not allowed to graze more cows on the meadow than they can care for over the winter — a rule that dates back to 1517. Ostrom has documented similar effective examples of ‘governing the commons’ in her research in Kenya, Guatemala, Nepal, Turkey, and Los Angeles. – Jay Walljasper, 2011

If someone is polluting air, we have governments to [shut](https://www.hindustantimes.com/mumbai-news/834-factories-across-maharashtra-shut-down-in-2-years-for-causing-pollution-mpcb/story-MrmmXa9XH9Vdkzu2wKSdcL.html) [them](https://www.npr.org/sections/parallels/2017/10/23/559009961/china-shuts-down-tens-of-thousands-of-factories-in-unprecedented-pollution-crack) [down](https://www.ndtv.com/india-news/pollution-inspected-1-534-places-issued-closure-notices-to-228-factories-construction-sites-in-delhi-ncr-panel-tells-sc-2645904).
Same if someone is damaging rivers.
And so on.

Even in smaller commons, we set up rules to control access to the commons such that there is no over-utilization. For example, in a library, you get to take only one or two books home at once and you've got to return them. In a park, you're allowed to only play, walk, run, etc, but not have picnic or damage property. And so on.

Basically, "governance" is the solution to the so-called tragedy of the commons. And it exists almost everywhere.

### Does the tragedy of the commons apply to open-source software?

Let us try to see if it fits.

Take software. A copy of a program has nearly zero marginal cost. This is the [fundamental reason free software movement exists](https://www.gnu.org/philosophy/shouldbefree.en.html). Which means it is impossible to over-utilize software. Therefore, you cannot ruin software by over-utilizing it. You cannot apply the tragedy of the commons to software itself.

But take the developers or maintainers of such software. They have limited energy. If you consider their skills and time as "commons", then it is indeed possible to over-utilize those to the detriment of the commons and ruin it. You can waste developer time by creating wrong or misleading bug reports. You can destroy their morale by shitting on them or making money off their work or taking credit of it.

So, yes, the tragedy of the commons can apply to open-source software developers.

And so should the criticism of the tragedy of commons too!

### Solutions applied to open-source software developers

How do you [nourish the commons](https://feminist-software.gitlab.io/commons-handbook/nourishing/) that is open-source software developers?

One challenge we will face is that unlike grassland we are dealing with human beings. And it is difficult to predict how human beings will respond to any stimulus. Yet there are several governance mechanisms we can try:

* Adopting code of conduct to avoid people using violent language.
* Adopting copyleft licenses to prevent people from using commons without contributing back to commons.
* Using social pressure and norms to ensure monetary contributions to developers.
* Encouraging collectivization so that different people can handle different tasks avoiding burnout.
* ...
* ...

Despite all of this people can still feel burnout. This can be considered normal wear and tear of the commons. For all we know, people can give up on open-source even if things are going absolutely fine — they could get busy, they might [move to farming](https://github.com/dylanaraps), they might be in a war. There are things that we can prevent and things that are out of our control.

### Abundance mindset

Unlike natural resources, the pool of open-source software developers is not shrinking. We can train new people and increase the size of this pool. We can improve the skills of those who are already in the pool. We can have various other kind of contributors (design, communication, testing, documentation) working closely with coders improving their efficiency and making it a more interesting team.

And since these are human beings, they can also be motivated by seeing the impact of their work. They can be more productive by doing good and this can be a virtuous cycle.

And by looking at the work of some developers, other developers learn too. "Standing on the shoulders of giants" truly applies here.

### What really is the problem?

In discussions surrounding these, people fail to cut through cruft and see the real problem. [The spirit of commons](https://asd.learnlearn.in/remaking-capitalism/) is almost always wrecked by the mean-spirit of capitalism. It is capitalists that try to maximize profit by stealing the work of open-source developers. It is capitalism that forces people on the mad hustle for money.

The real problem, then, is the [tragedy of the market](https://centerforneweconomics.org/apply/the-commons-program/what-about-the-tragedy-of-the-commons/).